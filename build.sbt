val thisScalaVersion = "2.12.6"

val root = Project("root", file("."))
  .settings(
    name := "schibsted_test",
    version := "0.1.0",
    scalaVersion := thisScalaVersion,

    libraryDependencies ++= Seq(
      "org.scalamock" %% "scalamock" % "4.1.0" % Test,
      "org.scalatest" %% "scalatest" % "3.0.4" % Test,
    )
  )
