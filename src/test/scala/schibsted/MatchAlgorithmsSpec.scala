package schibsted

import org.scalatest.{FreeSpec, Matchers}
import MatchAlgorithm.{splitIntoWords, calcCollationKey, calculateWordsMatchRatio, calcCollationKeySet}

class MatchAlgorithmsSpec extends FreeSpec with Matchers {

  "splitIntoWords" - {
    "Should split by special character and space characters" in {
      splitIntoWords("red,dog|black&shéép\nand\tmore") shouldEqual Seq("red", "dog", "black", "shéép", "and", "more")
    }

    "multiple space/special characters" in {
      splitIntoWords("brown || & fox") shouldEqual Seq("brown", "fox")
    }

    "Should include diacritic letters as part of a word" in {
      splitIntoWords("shéép\nEl Niño") shouldEqual Seq("shéép", "El", "Niño")
    }

    "Should consider numbers as part of a word" in {
      splitIntoWords("kākā0 and 5") shouldEqual Seq("kākā0", "and", "5")
    }
  }

  "relaxedStringOrdering" - {
    "is case insensitive" in {
      calcCollationKey("ABC") shouldEqual calcCollationKey("abc")
    }

    "ignores diacritics" in {
      calcCollationKey("café") shouldEqual calcCollationKey("cafe")
      calcCollationKey("ž") shouldEqual calcCollationKey("Z")
    }
  }

  "calculateRank" - {
    "give a percentage of words matching" in {
      calculateWordsMatchRatio(calcCollationKeySet("dkdk café"), List("asdf", "cafe")) shouldEqual 0.5
    }

    "gives percentage of words matching (diacritic)" in {
      calculateWordsMatchRatio(calcCollationKeySet("café France"), List("cafe")) shouldEqual 1
    }
  }

}
