package schibsted

import java.io.{ByteArrayInputStream, InputStream}
import java.nio.charset.StandardCharsets

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}
import schibsted.SearcherConsoleSpec.AccumulateLogger
import schibsted.searcher.Searcher
import schibsted.utils.Logger

class SearcherConsoleSpec extends FreeSpec with Matchers with MockFactory {
  "SearcherConsole" - {

    "Can handle empty input (EOF)" in {
      val inputs = makeInputStream() // empty
      val logger = new AccumulateLogger()

      new SearcherConsole(inputs, logger, _ => Vector.empty).run()

      logger.getOutput shouldEqual Seq(
        "search> ",
        "Good bye!",
        ""
      ).mkString("\n")
    }

    "Can handle search queries (both when there are search results or not)" in {
      val inputs = makeInputStream(
        "hello world",
        "goodbye world"
      )
      val logger = new AccumulateLogger()

      val searcher = mock[Searcher]

      inSequence {
        (searcher.search _).expects(Seq("hello", "world")).returns(Vector.empty)
        (searcher.search _).expects(Seq("goodbye", "world")).returns(Vector(
          "file1" -> 0.5,
          "file2" -> 0.33
        ))
      }

      new SearcherConsole(inputs, logger, searcher).run()

      // Note that this is only the output from our logger and does not include
      // what's typed by the user (which the console also echos out)
      // Thus the formatting is slightly off but we can still validate our output
      // is correct
      logger.getOutput shouldEqual Seq(
        "search> [No match found]",
        "",
        "search> file1 50%",
        "file2 33%",
        "",
        "search> ",
        "Good bye!",
        ""
      ).mkString("\n")
    }
  }

  def makeInputStream(inputLines: String*): InputStream = {
    new ByteArrayInputStream(inputLines.mkString("\n").getBytes(StandardCharsets.UTF_8))
  }
}

object SearcherConsoleSpec {
  class AccumulateLogger() extends Logger {
    private var outputs = ""

    override def println(x: Any): Unit = {
      outputs = outputs concat x.toString concat "\n"
    }

    override def print(x: Any): Unit = {
      outputs = outputs concat x.toString
    }

    def getOutput: String = outputs
  }
}
