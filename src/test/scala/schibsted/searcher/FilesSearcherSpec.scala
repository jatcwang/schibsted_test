package schibsted.searcher

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files

import org.scalatest.{FreeSpec, Matchers}

class FilesSearcherSpec extends FreeSpec with Matchers {

  "search" - {

    "Returns only the top 10 results, sorted by score DESC then file name ASC" in {
      val res = searcher.search(Seq("hello", "world"))
      res shouldEqual Vector(
        fileNames(0) -> 1,
        fileNames(9) -> 1,
        fileNames(1) -> 0.5,
        fileNames(2) -> 0.5,
        fileNames(3) -> 0.5,
        fileNames(4) -> 0.5,
        fileNames(5) -> 0.5,
        fileNames(6) -> 0.5,
        fileNames(7) -> 0.5,
        fileNames(8) -> 0.5,
      )
    }

    "Returns empty if nothing is found" in {
      val res = searcher.search(Seq("invalid"))
      res shouldEqual Vector.empty
    }

  }

  lazy val searcher = new FilesSearcher(files)

  lazy val files = {
    Seq(
      fileWithContent("00", "hello world"),
      fileWithContent("01", "hello long"),
      fileWithContent("02", "hello short"),
      fileWithContent("03", "hello temp"),
      fileWithContent("04", "hello string"),
      fileWithContent("05", "hello dog"),
      fileWithContent("06", "hello cat"),
      fileWithContent("07", "hello horse"),
      fileWithContent("08", "hello sun"),
      fileWithContent("09", "hello goodbye world"),
      fileWithContent("10", "find the easter egg hello"),
      fileWithContent("11", "it may or may not exist hello"),
    )
  }

  lazy val fileNames = files.map(_.getName)

  def fileWithContent(extraPrefix: String, content: String): File = {
    val f = Files.createTempFile(s"file$extraPrefix", "txt")
    Files.write(f, content.getBytes(StandardCharsets.UTF_8))
    f.toFile
  }

}
