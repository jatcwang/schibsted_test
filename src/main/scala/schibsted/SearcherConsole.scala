package schibsted

import java.io.InputStream
import java.util.Scanner

import schibsted.utils.Logger
import schibsted.MatchAlgorithm.splitIntoWords
import schibsted.searcher.Searcher

/**
  * Logic for console interaction
  */
class SearcherConsole(inputStream: InputStream, logger: Logger, searcher: Searcher) {
  val scanner = new Scanner(inputStream)

  def run() {
    var continue = true
    while (continue) {
      try {
        logger.print("search> ")
        val input = scanner.nextLine()
        val searchWords = splitIntoWords(input)
        val fileScores = searcher.search(searchWords)
        if (fileScores.nonEmpty) {
          fileScores.foreach { case (fileName, score) =>
            logger.println(s"$fileName ${(score * 100).round.toString}%")
          }
        }
        else {
          logger.println("[No match found]")
        }
        logger.println("")
      } catch {
        case _: NoSuchElementException => continue = false
      }
    }
    logger.println("\nGood bye!")
  }
}
