package schibsted.searcher

trait Searcher {
  def search(searchWords: Seq[String]): Vector[(String, Double)]
}
