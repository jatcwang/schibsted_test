package schibsted.searcher

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.text.CollationKey

import schibsted.MatchAlgorithm.{calcCollationKeySet, calculateWordsMatchRatio}

/**
  * A file searcher that performs search over the given list of files.
  * Provided files must exist and be UTF-8 encoded
  */
class FilesSearcher(files: Seq[File]) extends Searcher {

  // Read all file contents into memory, assuming UTF-8 encoding
  val fileNameToCollKeySet = files.map { f =>
    val bytes = Files.readAllBytes(f.toPath)
    val content = new String(bytes, StandardCharsets.UTF_8)
    f.getName -> calcCollationKeySet(content)
  }.toMap

  private val orderingTopScoreThenFileName: Ordering[(Double, String)] =
    Ordering.Tuple2(implicitly[Ordering[Double]].reverse, implicitly[Ordering[String]])

  override def search(searchWords: Seq[String]): Vector[(String, Double)] = {
    searchAndSortByScore(fileNameToCollKeySet, searchWords)
  }

  private def searchAndSortByScore(filesToCollKeySet: Map[String, Set[CollationKey]], searchWords: Seq[String]): Vector[(String, Double)] = {
    filesToCollKeySet.mapValues(contentKeySet => calculateWordsMatchRatio(contentKeySet, searchWords))
      .toVector
      .sortBy { case (fileName, score) => (score, fileName) }(orderingTopScoreThenFileName)
      .take(10)
      .collect {
        case (fileName, score) if score > 0 => fileName -> score
      }
  }
}

