package schibsted

import java.io.File

import schibsted.searcher.FilesSearcher
import schibsted.utils.ConsoleLogger

object SearcherApp {
  def main(args: Array[String]): Unit = {
    val files = new File(args(0)).listFiles(_.isFile)
    val searcher = new FilesSearcher(files)

    println(s"${files.length} files read into memory")

    val searcherConsole = new SearcherConsole(System.in, ConsoleLogger, searcher)

    searcherConsole.run()
  }

}
