package schibsted
import java.text.{CollationKey, Collator}
import java.util.Locale

import scala.collection.{mutable => mut}

/**
  * Algorithms for word splitting and matching.
  *
  */
object MatchAlgorithm {

  /** Characters are collated into "primary" strength,
    * which means that diacritics and upper/lowercase are ignored */
  private val collator = {
    val c = Collator.getInstance(Locale.ROOT)
    c.setStrength(Collator.PRIMARY)
    c
  }

  /**
    * Word splitting lumps UTF-8 alphanumeric characters together
    */
  val WORD_SPLIT_PATTERN = """[^\p{IsAlphabetic}0-9]+"""

  def calcCollationKey(str: String): CollationKey = {
    collator.getCollationKey(str)
  }

  def splitIntoWords(content: String): Seq[String] = {
    // IsAlphabetic case is unicode-aware, and thus handles diacritic characters correctly
    // (consider them as a valid word character)
    content.split(WORD_SPLIT_PATTERN)
  }

  /**
    * Given string containing the content of a file, a Set of CollationKey
    * (thus removing duplicate words)
    */
  def calcCollationKeySet(content: String): Set[CollationKey] = {
    splitIntoWords(content).foldLeft(mut.Set.empty[CollationKey]) { case (keySet, str) =>
      keySet += calcCollationKey(str)
    }.toSet
  }

  /**
    * Find the ratio of search words that exist in the content to search through
    * @param contentKeySet Set of CollationKey to search through
    * @param searchWords words to search for
    */
  def calculateWordsMatchRatio(contentKeySet: Set[CollationKey], searchWords: Seq[String]): Double = {
    val searchKeySet = searchWords.map(calcCollationKey(_)).toSet
    val containedKeys = searchKeySet.intersect(contentKeySet)

    containedKeys.size.toDouble / searchKeySet.size
  }

}
