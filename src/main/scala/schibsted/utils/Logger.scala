package schibsted.utils

trait Logger {
  def println(x: Any)
  def print(x: Any)
}

object ConsoleLogger extends Logger {
  override def println(x: Any): Unit = Console.println(x)
  override def print(x: Any): Unit = Console.print(x)
}
