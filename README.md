# Building the JAR

```
./build
```

## Running the program

After building, you can run the program with provided example files

```
java -cp schibsted_test-assembly-0.1.0.jar schibsted.SearcherApp ./example_files
```

I think there's a mistake in the instruction PDF, 
as you don't specify the main class when using `java -jar` (`java -cp` is the one that allows you to specify the main class to run)
thus I cannot adhere to the exact command line interface.

See: https://stackoverflow.com/questions/5474666/how-to-run-a-class-from-jar-which-is-not-the-main-class-in-its-manifest-file
